PMD and FXCOP Rules
===================
A simple boilerplate rule set in 3 folders: 
- Android-Sample (showing sample android app with unit test cases) 
- Java-Sample (showing PMD rules for Java) 
- FXCOP-Sample (Showing FXCop Rules for ASP.NET Web Applications) 
